﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Breeze.Sharp;
using WebApplication1.Models;
namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        EntityManager entityManager = new EntityManager("http://angularsnobel.azurewebsites.net/breeze/breeze");
        Label tb;
        public MainWindow()
        {
            InitializeComponent();
            tb = new Label();
            tb.Content = "Loading...";
            statusBar.Items.Add(tb);
            // Create an EntityManager
           
        }
        public override async void BeginInit()
        {
            base.BeginInit();
            DataContext = this;
            Configuration.Instance.ProbeAssemblies(typeof(Patient).Assembly);
            
            //entityManager.MetadataStore.AllowedMetadataMismatchTypes = MetadataMismatchType.AllAllowable;
            // Attach an anonymous handler to the MetadataMismatch event



            Patients = await QueryPatientsFrom(entityManager);
            tb.Content = "Loaded!";
        }
        public async void Refresh() {
            Patients = await QueryPatientsFrom(entityManager);
        
        
        }
        private async Task<IEnumerable<Patient>> QueryPatientsFrom(EntityManager entityManager)
        {
            try
            {
                return await new EntityQuery<Patient>("Patients").Execute(entityManager);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.GetType().Name + ": " + e.Message);
                return new Patient[0];
            }
        }

        public IEnumerable<Patient> Patients
        {
            get { return _patients; }
            set
            {
                _patients = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Patients"));
                }
            }
        }
        private IEnumerable<Patient> _patients;

        public event PropertyChangedEventHandler PropertyChanged;

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            //display add patient dialog
            AddPatient ap = new AddPatient(entityManager);
            ap.Show();
        }
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            //display add patient dialog
            //this.Close();
            Application.Current.Shutdown();
        }
    }
} 
