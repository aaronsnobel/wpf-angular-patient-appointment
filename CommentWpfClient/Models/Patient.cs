﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Patient : Breeze.Sharp.BaseEntity
    {
        public int PatientId
        {
            get { return GetValue<int>(); }
            set { SetValue(value); }
        }

        public string PatientName
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }
        public Nullable<DateTime> PatientAppointment
        {
            get { return GetValue<DateTime>(); }
            set { SetValue(value); }
        }
        public string PatientPIN
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        } 
    }
}
