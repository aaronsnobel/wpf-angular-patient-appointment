﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApplication1.Models;

namespace WpfApplication1
{
    class MainViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Comment> Comments { get; set; }

        public MainViewModel()
        {
            Comments = new ObservableCollection<Comment>();
            Get();
        }

        public void Get()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:1121/breeze/breeze");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage resp = client.GetAsync("/Patients").Result;
            if (resp.IsSuccessStatusCode)
            {
                IEnumerable<Comment> comments = resp.Content.ReadAsAsync<IEnumerable<Comment>>().Result;
                foreach (Comment c in comments)
                    Comments.Add(c);
            }
        }

        private string _errorMessage;

        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
            set
            {
                _errorMessage = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ErrorMessage"));
                // Property Changed
            }
        }

        public bool CanSave(object obj)
        {
            return true;
        }

        public void Save(object obj)
        {

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:1121/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            ErrorMessage = "";
            bool fail = false;
            foreach (Comment c in Comments)
            {
                HttpResponseMessage resp = client.PutAsync<Comment>(String.Format("api/Comment/{0}", c.ID), c, new JsonMediaTypeFormatter()).Result;

                if (!resp.IsSuccessStatusCode)
                {
                    ErrorMessage += "Adding new item \n";
                    HttpResponseMessage resp2 = client.PostAsync<Comment>("api/Comment", c, new JsonMediaTypeFormatter()).Result;
                    if (!resp2.IsSuccessStatusCode)
                    {
                        ErrorMessage += "Error in updating " + c.ID.ToString() + "\n";
                        fail = true;
                    }
                    else
                    {
                        Comment addedComment = resp2.Content.ReadAsAsync<Comment>().Result;
                        c.ID = addedComment.ID;
                    }
                }
            }

            if (!fail)
                ErrorMessage += "Update is successful\n";
        }

        public bool CanDelete(object obj)
        {
            return true;
        }

        public void Delete(object obj)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:1121/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


            if (obj == null)
                return;

            int id = (int)obj;

            HttpResponseMessage resp = client.DeleteAsync(String.Format("api/Comment/{0}", id)).Result;

            ErrorMessage = "Error in delete";
            if (resp.IsSuccessStatusCode)
            {
                var qry = from c in Comments
                          where c.ID == id
                          select c;
                Comment cmt = qry.First<Comment>();
                bool deleted = false;
                if (cmt != null)
                    deleted = Comments.Remove(cmt);

                if(deleted)
                    ErrorMessage = "Delete successful";
            }
            
        }


        private BaseCommand _saveCommand;

        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new BaseCommand(Save, CanSave);
                }

                return _saveCommand;
            }
        }

        private BaseCommand _deleteCommand;

        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new BaseCommand(Delete, CanDelete);
                }
                return _deleteCommand;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}