﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Breeze.Sharp;
using WebApplication1.Models;
namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for AddPatient.xaml
    /// </summary>
    public partial class AddPatient : Window
    {
        EntityManager em;
        public AddPatient(EntityManager em)
        {
            InitializeComponent();
            this.em = em;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            Patient pt = new Patient();
            pt.PatientAppointment = txtDate.SelectedDate;
            pt.PatientName = txtPatientName.Text;
            pt.PatientPIN = random.Next(1000, 9999).ToString();
            em.AddEntity(pt);
            em.SaveChanges();

        }

        private void btnCancel_Click(object s, RoutedEventArgs e)
        {
            Close();
        
        }
    
    
    
    }



}
