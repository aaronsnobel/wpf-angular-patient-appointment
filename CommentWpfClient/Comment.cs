﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WpfApplication1.Models
{
    public enum AuthType
    {
        Facebook,
        LinkedIn,
        Twitter,
        Google
    }

    public class Comment : INotifyPropertyChanged
    {
        private int _ID = 0;
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
                if(PropertyChanged!=null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
        }

        private string _author = "";
        public string Author 
        {
            get
            {
                return _author;
            }
            set 
            {
                _author = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Author"));
            }

        }

       

        private string _text = default(string);
        public string Text 
        { 
            get
            {
                return _text;
            }
            set
            {
                _text = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Text"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}